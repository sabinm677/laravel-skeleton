<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test', function () {
    header('Access-Control-Allow-Origin: *');
    return ['name' => 'abc'];
});


// Route::middleware('cors')->match(['post', 'options'], 'test', function (\Illuminate\Http\Request $request) {
    // // header('Access-Control-Allow-Origin: *');
    // dd($request->input());
    
    // return ['name' => 'abc'];
// });
Route::middleware('cors')->post('test', function (\Illuminate\Http\Request $request) {
    // header('Access-Control-Allow-Origin: *');
    // dd($request->input());
    
    return ['name' => 'abc'];
});
