<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{

    public function children()
    {
        return $this->hasMany('App\Action');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }
    
}
